<?php
   
   namespace app\assets;


   use yii\web\AssetBundle;
   use yii\web\View;



   class HomeAsset extends AssetBundle {

      public $basePath = '@webroot';
      public $baseUrl = '@web';
      public $css =[
        'css/style.css',
        'css/letras.css',
        'css/venobox.css',
        'css/venobox.min.css',
        'css/boxicons/animations.css',
        'css/boxicons/boxicons.min.css',
        'css/boxicons/transformations.css',
        'css/aos.css',
        'css/carousel/owl.carousel.css',



      ];
      public $js = [
        //'js/main.js',
        'js/letras.js',
        //'js/counterup.min.js',
        //'js/venobox.js',
        //'js/venobox.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',
        'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js',
        //'js/aos.js'
        'js/fontawesome.5.3.1.js'

        
        //'js/loader.js',
      ];
      public  $jsOptions = ['position' => View::POS_HEAD];
      	
      public $depends = [
      	'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        //'yii\web\JqueryAsset',
    ];
   }