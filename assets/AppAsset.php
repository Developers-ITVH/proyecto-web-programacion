<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;


/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/navbar.css',
        //'css/letras.css',
        //'css/galeria.css'
    ];
    public $js = [
        //'js/letras.js',
        //'js/galeria.js',
        //'js/fontAwesome.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js',
        //'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.6.1/gsap.min.js'
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];

}
