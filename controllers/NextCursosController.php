<?php

namespace app\controllers;

use Yii;
use app\models\NextCursos;
use app\models\NextEncargados;
use app\models\CatEstatus;
use app\models\NextCursosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\models\UploadForm;

/**
 * NextCursosController implements the CRUD actions for NextCursos model.
 */
class NextCursosController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class'   => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all NextCursos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NextCursosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NextCursos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NextCursos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {   
        $model = new NextCursos();
        $encargado = ArrayHelper::map(NextEncargados::find()->all(), 'enc_id', 'NombreCompleto');
        $estatus = ArrayHelper::map(CatEstatus::find()->all(), 'cat_idestatus', 'cat_nombre');

        if ($model->load(Yii::$app->request->post())) {
           $ruta = $model->cur_foto;
       
        $fotito = UploadedFile::getInstance($model, 'cur_foto');
            if (!is_null($fotito)) {
                $rutainicial = 'images/cursos/';
                $nombre = strtolower(str_replace(' ', '', $model->cur_nombre));
                $ext = explode('.', $fotito->name);
                $ext = $ext[count($ext)-1];
                $rutadestino = $rutainicial.$nombre.'.'.$ext;       
                    if($fotito->saveAs($rutadestino)){
                       $model->cur_foto = $rutadestino;
                    }

                    }else {
                        //Si no cambiaremos la imagen asignamos el valor guardado anteriormente
                        $model->cur_foto = $ruta;
                    }
        
                $model->save();

                return $this->redirect(['view','id'=>$model->cur_id]);
            }

                return $this->render('create', [
                    'model' => $model,
                    'encargado' => $encargado,
                    'estatus' => $estatus
                ]);
            
    }
    /**
     * Updates an existing NextCursos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $encargado = ArrayHelper::map(NextEncargados::find()->all(), 'enc_id', 'NombreCompleto');
        $estatus = ArrayHelper::map(CatEstatus::find()->all(), 'cat_idestatus', 'cat_nombre');

        if ($model->load(Yii::$app->request->post())) {
           $ruta = $model->cur_foto;
       
        $fotito = UploadedFile::getInstance($model, 'cur_foto');
            if (!is_null($fotito)) {
                $rutainicial = 'images/cursos/';
                $nombre = strtolower(str_replace(' ', '', $model->cur_nombre));
                $ext = explode('.', $fotito->name);
                $ext = $ext[count($ext)-1];
                $rutadestino = $rutainicial.$nombre.'.'.$ext;       
                if($fotito->saveAs($rutadestino)){
                   $model->cur_foto = $rutadestino;
                }
            }else {
                    //Si no cambiaremos la imagen asignamos el valor guardado anteriormente
                    $model->cur_foto = $ruta;
                }
    
                $model->save();
                return $this->redirect(['view','id'=>$model->cur_id]);
        }
        return $this->render('update', [
            'model' => $model,
            'encargado' => $encargado,
            'estatus' => $estatus
        ]);
    }

    /**
     * Deletes an existing NextCursos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NextCursos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NextCursos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NextCursos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionPreregistro($id)
    {         
        $todos = NextCursos::find()->all();
        $curso = NextCursos::findOne(['cur_id'=>$id]);
        return $this->render('preregistro',compact('curso','todos'));
    }
}
