<?php

namespace app\controllers;

use Yii;
use app\models\NextEncargados;
use app\models\NextEncargadosSearch;
use app\models\CatGenero;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;


/**
 * NextEncargadosController implements the CRUD actions for NextEncargados model.
 */
class NextEncargadosController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class'   => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all NextEncargados models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NextEncargadosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NextEncargados model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NextEncargados model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NextEncargados();
        $user= new User();
        $genero = ArrayHelper::map(CatGenero::find()->all(), 'cat_idgenero', 'cat_nombre');

       if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            $user['username'] = $user['email'];
            $user->save();
            $model->enc_fkuserw = $user['id'];
            $model->save();

            //$user::assignRole($user->id, 'cliente');
             return $this->redirect(['view','id'=>$model->enc_id]);
        }
        return $this->render('create', [
            'model' => $model,
             'user' => $user,
            'genero' => $genero
        ]);
    }

    /**
     * Updates an existing NextEncargados model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $user = User::find()->where(['id' => $model['enc_fkuserw']])->one();
        $genero = ArrayHelper::map(CatGenero::find()->all(), 'cat_idgenero', 'cat_nombre');

        if($model->load(\Yii::$app->getRequest()->post()) && $user->load(\Yii::$app->getRequest()->post())) {
            $user['username'] = $user['email'];
            $user->save();
            $model->enc_fkuserw = $user['id'];
            $model->save();
            return $this->redirect(['view', 'id' => $model->enc_id]);
        }

        return $this->render('update', [
            'model' => $model,
            'user' => $user,
            'genero' => $genero
        ]);
    }

    /**
     * Deletes an existing NextEncargados model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NextEncargados model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NextEncargados the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NextEncargados::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
