<?php

namespace app\controllers;

use Yii;
use app\models\NextEstudiantes;
use app\models\NextEstudiantesSearch;
use app\models\NextInscripcion;
use app\models\NextCursos;
use app\models\CatGenero;
use app\models\NextCarrera;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use webvimark\modules\UserManagement\models\User;
use yii\helpers\ArrayHelper;

/**
 * NextEstudiantesController implements the CRUD actions for NextEstudiantes model.
 */
class NextEstudiantesController extends Controller
{
    
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ghost-access'=> [
                'class'   => 'webvimark\modules\UserManagement\components\GhostAccessControl',
            ],
        ];
    }

    /**
     * Lists all NextEstudiantes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NextEstudiantesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single NextEstudiantes model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new NextEstudiantes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NextEstudiantes();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->est_id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing NextEstudiantes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->est_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing NextEstudiantes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NextEstudiantes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NextEstudiantes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NextEstudiantes::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionRegistro()
    {   
        $model = new \app\models\NextEstudiantes();
        $user= new User();
        $genero = ArrayHelper::map(CatGenero::find()->all(), 'cat_idgenero', 'cat_nombre');
        $carrera = ArrayHelper::map(NextCarrera::find()->all(), 'car_id', 'car_nombre');


        if ($model->load(Yii::$app->request->post()) && $user->load(Yii::$app->request->post())) {
            $user['username'] = $user['email'];
            $user->save();
            $model->est_fkuserw = $user['id'];
            $model->est_correo = $user['email'];
            $model->save();

        //$user::assignRole($user->id, 'cliente');
        return $this->redirect(['view','id'=>$model->est_id]);

        }

        return $this->render('registro', [
            'model' => $model,
            'user' => $user,
            'genero' => $genero,
            'carrera' => $carrera,

        ]);
    }

    public function actionInscripcion($id)
    {   
        $estudiante = NextEstudiantes::find()->where(['est_fkuserw' => Yii::$app->user->identity->id])->one();
        $identity = $estudiante->est_id;
        $inscripcion = new NextInscripcion();
        $inscripcion->inc_fkcurso = $id;
        $inscripcion->inc_fkestudiante = $identity;
        $inscripcion->save();

        return $this->redirect(['next-inscripcion/view','id'=>$inscripcion->inc_id]);
    }
}
