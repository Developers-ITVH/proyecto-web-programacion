<?php

namespace app\models;

use Yii;
use webvimark\modules\UserManagement\models\User;

/**
 * This is the model class for table "next_estudiantes".
 *
 * @property int $est_id
 * @property string|null $est_nombre
 * @property string|null $est_paterno
 * @property string|null $est_materno
 * @property string|null $est_matricula
 * @property string|null $est_correo
 * @property int|null $est_fkcarrera
 * @property int|null $est_fkgenero
 * @property int|null $est_fkuserw
 *
 * @property NextAsistencias[] $nextAsistencias
 * @property NextCarrera $estFkcarrera
 * @property CatGenero $estFkgenero
 * @property User $estFkuserw
 * @property NextInscripcion[] $nextInscripcions
 */
class NextEstudiantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_estudiantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['est_fkcarrera', 'est_fkgenero', 'est_fkuserw'], 'integer'],
            [['est_nombre', 'est_paterno', 'est_materno', 'est_matricula', 'est_correo'], 'string', 'max' => 255],
            [['est_fkcarrera'], 'exist', 'skipOnError' => true, 'targetClass' => NextCarrera::className(), 'targetAttribute' => ['est_fkcarrera' => 'car_id']],
            [['est_fkgenero'], 'exist', 'skipOnError' => true, 'targetClass' => CatGenero::className(), 'targetAttribute' => ['est_fkgenero' => 'cat_idgenero']],
            [['est_fkuserw'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['est_fkuserw' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'est_id' => 'Est ID',
            'est_nombre' => 'Nombres',
            'est_paterno' => 'Apellido Paterno',
            'est_materno' => 'Apellido Materno',
            'est_matricula' => 'Matricula',
            'est_correo' => 'Correo',
            'est_fkcarrera' => 'Carrera',
            'est_fkgenero' => 'Genero',
            'est_fkuserw' => 'Est Fkuserw',
        ];
    }

    /**
     * Gets query for [[NextAsistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextAsistencias()
    {
        return $this->hasMany(NextAsistencias::className(), ['asi_fkestudiante' => 'est_id']);
    }

    /**
     * Gets query for [[EstFkcarrera]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstFkcarrera()
    {
        return $this->hasOne(NextCarrera::className(), ['car_id' => 'est_fkcarrera']);
    }

    public function getCarrera()
    {
        return $this->estFkcarrera->car_nombre;
    }

    /**
     * Gets query for [[EstFkgenero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstFkgenero()
    {
        return $this->hasOne(CatGenero::className(), ['cat_idgenero' => 'est_fkgenero']);
    }

    public function getGenero()
    {
        return $this->estFkgenero->cat_nombre;
    }

    /**
     * Gets query for [[EstFkuserw]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEstFkuserw()
    {
        return $this->hasOne(User::className(), ['id' => 'est_fkuserw']);
    }

    /**
     * Gets query for [[NextInscripcions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextInscripciones()
    {
        return $this->hasMany(NextInscripcion::className(), ['inc_fkestudiante' => 'est_id']);
    }
}
