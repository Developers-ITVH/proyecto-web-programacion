<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextCursos;

/**
 * NextCursosSearch represents the model behind the search form of `app\models\NextCursos`.
 */
class NextCursosSearch extends NextCursos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cur_id', 'cur_fkestatus', 'cur_fkencargados', 'cur_asistenciasestatus'], 'integer'],
            [['cur_nombre', 'cur_descripcion', 'cur_foto', 'cur_horaInicial', 'cur_horaFinal', 'cur_fechaInicial', 'cur_fechaFinal', 'cur_fechaCreacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextCursos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cur_id' => $this->cur_id,
            'cur_fkestatus' => $this->cur_fkestatus,
            'cur_fkencargados' => $this->cur_fkencargados,
            'cur_asistenciasestatus' => $this->cur_asistenciasestatus,
        ]);

        $query->andFilterWhere(['like', 'cur_nombre', $this->cur_nombre])
            ->andFilterWhere(['like', 'cur_descripcion', $this->cur_descripcion])
            ->andFilterWhere(['like', 'cur_foto', $this->cur_foto])
            ->andFilterWhere(['like', 'cur_horaInicial', $this->cur_horaInicial])
            ->andFilterWhere(['like', 'cur_horaFinal', $this->cur_horaFinal])
            ->andFilterWhere(['like', 'cur_fechaInicial', $this->cur_fechaInicial])
            ->andFilterWhere(['like', 'cur_fechaFinal', $this->cur_fechaFinal])
            ->andFilterWhere(['like', 'cur_fechaCreacion', $this->cur_fechaCreacion]);

        return $dataProvider;
    }
}
