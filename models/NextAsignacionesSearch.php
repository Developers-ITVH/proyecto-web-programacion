<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextAsignaciones;

/**
 * NextAsignacionesSearch represents the model behind the search form of `app\models\NextAsignaciones`.
 */
class NextAsignacionesSearch extends NextAsignaciones
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asg_id', 'asg_fkestatus', 'asg_fkcursos'], 'integer'],
            [['asg_nombre', 'asg_indicaciones', 'asg_url', 'asg_fecha'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextAsignaciones::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'asg_id' => $this->asg_id,
            'asg_fecha' => $this->asg_fecha,
            'asg_fkestatus' => $this->asg_fkestatus,
            'asg_fkcursos' => $this->asg_fkcursos,
        ]);

        $query->andFilterWhere(['like', 'asg_nombre', $this->asg_nombre])
            ->andFilterWhere(['like', 'asg_indicaciones', $this->asg_indicaciones])
            ->andFilterWhere(['like', 'asg_url', $this->asg_url]);

        return $dataProvider;
    }
}
