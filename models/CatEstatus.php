<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cat_estatus".
 *
 * @property int $cat_idestatus
 * @property string|null $cat_nombre
 *
 * @property NextAsignaciones[] $nextAsignaciones
 * @property NextCursos[] $nextCursos
 */
class CatEstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_estatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_idestatus' => 'Cat Idestatus',
            'cat_nombre' => 'Cat Nombre',
        ];
    }

    /**
     * Gets query for [[NextAsignaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextAsignaciones()
    {
        return $this->hasMany(NextAsignaciones::className(), ['asg_fkestatus' => 'cat_idestatus']);
    }

    /**
     * Gets query for [[NextCursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextCursos()
    {
        return $this->hasMany(NextCursos::className(), ['cur_fkestatus' => 'cat_idestatus']);
    }
}
