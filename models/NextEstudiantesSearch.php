<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextEstudiantes;

/**
 * NextEstudiantesSearch represents the model behind the search form of `app\models\NextEstudiantes`.
 */
class NextEstudiantesSearch extends NextEstudiantes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['est_id', 'est_fkcarrera', 'est_fkgenero', 'est_fkuserw'], 'integer'],
            [['est_nombre', 'est_paterno', 'est_materno', 'est_matricula', 'est_correo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextEstudiantes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'est_id' => $this->est_id,
            'est_fkcarrera' => $this->est_fkcarrera,
            'est_fkgenero' => $this->est_fkgenero,
            'est_fkuserw' => $this->est_fkuserw,
        ]);

        $query->andFilterWhere(['like', 'est_nombre', $this->est_nombre])
            ->andFilterWhere(['like', 'est_paterno', $this->est_paterno])
            ->andFilterWhere(['like', 'est_materno', $this->est_materno])
            ->andFilterWhere(['like', 'est_matricula', $this->est_matricula])
            ->andFilterWhere(['like', 'est_correo', $this->est_correo]);

        return $dataProvider;
    }
}
