<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_carrera".
 *
 * @property int $car_id
 * @property string|null $car_nombre
 *
 * @property NextEstudiantes[] $nextEstudiantes
 */
class NextCarrera extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_carrera';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['car_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'car_id' => 'Car ID',
            'car_nombre' => 'Car Nombre',
        ];
    }

    /**
     * Gets query for [[NextEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextEstudiantes()
    {
        return $this->hasMany(NextEstudiantes::className(), ['est_fkcarrera' => 'car_id']);
    }
}
