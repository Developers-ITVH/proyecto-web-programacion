<?php

namespace app\models;

use Yii;
use webvimark\modules\UserManagement\models\User;
/**
 * This is the model class for table "next_encargados".
 *
 * @property int $enc_id
 * @property string|null $enc_nombre
 * @property string|null $enc_paterno
 * @property string|null $enc_materno
 * @property int|null $enc_fkgenero
 * @property int|null $enc_fkuserw
 *
 * @property NextCursos[] $nextCursos
 * @property User $encFkuserw
 * @property CatGenero $encFkgenero
 */
class NextEncargados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_encargados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enc_fkgenero', 'enc_fkuserw'], 'integer'],
            [['enc_nombre', 'enc_paterno', 'enc_materno'], 'string', 'max' => 255],
            [['enc_fkuserw'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['enc_fkuserw' => 'id']],
            [['enc_fkgenero'], 'exist', 'skipOnError' => true, 'targetClass' => CatGenero::className(), 'targetAttribute' => ['enc_fkgenero' => 'cat_idgenero']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'enc_id' => 'Enc ID',
            'enc_nombre' => 'Nombres',
            'enc_paterno' => 'Apellido Paterno',
            'enc_materno' => 'Apellido Materno',
            'enc_fkgenero' => 'Genero',
            'enc_fkuserw' => 'Enc Fkuserw',
        ];
    }

    /**
     * Gets query for [[NextCursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextCursos()
    {
        return $this->hasMany(NextCursos::className(), ['cur_fkencargados' => 'enc_id']);
    }

    /**
     * Gets query for [[EncFkuserw]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEncFkuserw()
    {
        return $this->hasOne(User::className(), ['id' => 'enc_fkuserw']);
    }

    /**
     * Gets query for [[EncFkgenero]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEncFkgenero()
    {
        return $this->hasOne(CatGenero::className(), ['cat_idgenero' => 'enc_fkgenero']);
    }
    public function getGenero()
    {
        return $this->encFkgenero->cat_nombre;
    }

    public function getNombreCompleto()
    {
        return $this->enc_nombre.' '.$this->enc_paterno.' '.$this->enc_materno;
    }
}
