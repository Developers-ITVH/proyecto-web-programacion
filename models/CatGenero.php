<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cat_genero".
 *
 * @property int $cat_idgenero
 * @property string|null $cat_nombre
 *
 * @property NextEncargados[] $nextEncargados
 * @property NextEstudiantes[] $nextEstudiantes
 */
class CatGenero extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cat_genero';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_nombre'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cat_idgenero' => 'Cat Idgenero',
            'cat_nombre' => 'Cat Nombre',
        ];
    }

    /**
     * Gets query for [[NextEncargados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextEncargados()
    {
        return $this->hasMany(NextEncargados::className(), ['enc_fkgenero' => 'cat_idgenero']);
    }

    /**
     * Gets query for [[NextEstudiantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextEstudiantes()
    {
        return $this->hasMany(NextEstudiantes::className(), ['est_fkgenero' => 'cat_idgenero']);
    }
}
