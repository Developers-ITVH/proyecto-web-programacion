<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_cursos".
 *
 * @property int $cur_id
 * @property string|null $cur_nombre
 * @property string|null $cur_descripcion
 * @property string|null $cur_foto
 * @property string|null $cur_horario
 * @property string|null $cur_fechaInicial
 * @property string|null $cur_fechaFinal
 * @property string|null $cur_fechaCreacion
 * @property int|null $cur_fkestatus
 * @property int|null $cur_fkencargados
 * @property int|null $cur_asistenciasestatus
 *
 * @property NextAsignaciones[] $nextAsignaciones
 * @property NextAsistencias[] $nextAsistencias
 * @property CatEstatus $curFkestatus
 * @property NextEncargados $curFkencargados
 * @property NextEvidencias[] $nextEvidencias
 * @property NextInscripcion[] $nextInscripcions
 */
class NextCursos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_cursos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cur_descripcion'], 'string'],
            [['cur_fkestatus', 'cur_fkencargados', 'cur_asistenciasestatus'], 'integer'],
            [['cur_nombre', 'cur_foto', 'cur_horaInicial', 'cur_horaFinal', 'cur_fechaInicial', 'cur_fechaFinal'], 'string', 'max' => 255],
            [['cur_fechaCreacion'], 'safe'],
            [['cur_fkestatus'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['cur_fkestatus' => 'cat_idestatus']],
            [['cur_fkencargados'], 'exist', 'skipOnError' => true, 'targetClass' => NextEncargados::className(), 'targetAttribute' => ['cur_fkencargados' => 'enc_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cur_id' => 'ID',
            'cur_nombre' => 'Nombre del curso',
            'cur_descripcion' => 'Descripcion del curso',
            'cur_foto' => 'Foto del curso',
            'cur_horaInicial' => 'Hora inicial',
            'cur_horaFinal' => 'Hora final',            
            'cur_fechaInicial' => 'Fecha inicial',
            'cur_fechaFinal' => 'Fecha final',
            'cur_fechaCreacion' => 'Fecha creacion',
            'cur_fkestatus' => 'Cur Fkestatus',
            'cur_fkencargados' => 'Cur Fkencargados',
            'cur_asistenciasestatus' => 'Cur Asistenciasestatus',
        ];
    }

    /**
     * Gets query for [[NextAsignaciones]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextAsignaciones()
    {
        return $this->hasMany(NextAsignaciones::className(), ['asg_fkcursos' => 'cur_id']);
    }

    /**
     * Gets query for [[NextAsistencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextAsistencias()
    {
        return $this->hasMany(NextAsistencias::className(), ['asi_fkcurso' => 'cur_id']);
    }

    /**
     * Gets query for [[CurFkestatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurFkestatus()
    {
        return $this->hasOne(CatEstatus::className(), ['cat_idestatus' => 'cur_fkestatus']);
    }

    public function getEstatus()
    {
        return $this->curFkestatus->cat_nombre;
    }

    /**
     * Gets query for [[CurFkencargados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurFkencargados()
    {
        return $this->hasOne(NextEncargados::className(), ['enc_id' => 'cur_fkencargados']);
    }

    public function getEncargado()
    {
        return $this->curFkencargados->enc_nombre.' '.$this->curFkencargados->enc_paterno.' '.$this->curFkencargados->enc_materno;
    }

    /**
     * Gets query for [[NextEvidencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextEvidencias()
    {
        return $this->hasMany(NextEvidencias::className(), ['evi_fkcurso' => 'cur_id']);
    }

    /**
     * Gets query for [[NextInscripcions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNextInscripcions()
    {
        return $this->hasMany(NextInscripcion::className(), ['inc_fkcurso' => 'cur_id']);
    }
}
