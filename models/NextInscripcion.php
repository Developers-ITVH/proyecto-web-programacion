<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_inscripcion".
 *
 * @property int $inc_id
 * @property int|null $inc_fkcurso
 * @property int|null $inc_fkestudiante
 *
 * @property NextCursos $incFkcurso
 * @property NextEstudiantes $incFkestudiante
 */
class NextInscripcion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_inscripcion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inc_fkcurso', 'inc_fkestudiante'], 'integer'],
            [['inc_fkcurso'], 'exist', 'skipOnError' => true, 'targetClass' => NextCursos::className(), 'targetAttribute' => ['inc_fkcurso' => 'cur_id']],
            [['inc_fkestudiante'], 'exist', 'skipOnError' => true, 'targetClass' => NextEstudiantes::className(), 'targetAttribute' => ['inc_fkestudiante' => 'est_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'inc_id' => 'Inc ID',
            'inc_fkcurso' => 'Inc Fkcurso',
            'inc_fkestudiante' => 'Inc Fkestudiante',
        ];
    }

    /**
     * Gets query for [[IncFkcurso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncFkcurso()
    {
        return $this->hasOne(NextCursos::className(), ['cur_id' => 'inc_fkcurso']);
    }

    public function getCurso()
    {
        return $this->incFkcurso->cur_nombre;
    }

    /**
     * Gets query for [[IncFkestudiante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncFkestudiante()
    {
        return $this->hasOne(NextEstudiantes::className(), ['est_id' => 'inc_fkestudiante']);
    }

    public function getNombrecom()
    {
        return $this->incFkestudiante->est_nombre.' '.$this->incFkestudiante->est_paterno.' '.$this->incFkestudiante->est_materno;
    }
}
