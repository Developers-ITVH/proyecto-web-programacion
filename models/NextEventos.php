<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_eventos".
 *
 * @property int $eve_id
 * @property string|null $eve_nombre
 * @property string|null $eve_ruta
 */
class NextEventos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_eventos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eve_nombre', 'eve_ruta'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'eve_id' => 'Eve ID',
            'eve_nombre' => 'Eve Nombre',
            'eve_ruta' => 'Eve Ruta',
        ];
    }
}
