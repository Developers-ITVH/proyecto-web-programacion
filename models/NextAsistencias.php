<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_asistencias".
 *
 * @property int $asi_id
 * @property string|null $asi_horaAsistencia
 * @property string|null $asi_fechaAsistencia
 * @property int|null $asi_fkcurso
 * @property int|null $asi_fkestudiante
 *
 * @property NextCursos $asiFkcurso
 * @property NextEstudiantes $asiFkestudiante
 */
class NextAsistencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_asistencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asi_id'], 'required'],
            [['asi_id', 'asi_fkcurso', 'asi_fkestudiante'], 'integer'],
            [['asi_horaAsistencia', 'asi_fechaAsistencia'], 'safe'],
            [['asi_id'], 'unique'],
            [['asi_fkcurso'], 'exist', 'skipOnError' => true, 'targetClass' => NextCursos::className(), 'targetAttribute' => ['asi_fkcurso' => 'cur_id']],
            [['asi_fkestudiante'], 'exist', 'skipOnError' => true, 'targetClass' => NextEstudiantes::className(), 'targetAttribute' => ['asi_fkestudiante' => 'est_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'asi_id' => 'Asi ID',
            'asi_horaAsistencia' => 'Asi Hora Asistencia',
            'asi_fechaAsistencia' => 'Asi Fecha Asistencia',
            'asi_fkcurso' => 'Asi Fkcurso',
            'asi_fkestudiante' => 'Asi Fkestudiante',
        ];
    }

    /**
     * Gets query for [[AsiFkcurso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsiFkcurso()
    {
        return $this->hasOne(NextCursos::className(), ['cur_id' => 'asi_fkcurso']);
    }

    /**
     * Gets query for [[AsiFkestudiante]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsiFkestudiante()
    {
        return $this->hasOne(NextEstudiantes::className(), ['est_id' => 'asi_fkestudiante']);
    }
}
