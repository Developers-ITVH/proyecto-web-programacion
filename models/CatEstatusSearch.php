<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CatEstatus;

/**
 * CatEstatusSearch represents the model behind the search form of `app\models\CatEstatus`.
 */
class CatEstatusSearch extends CatEstatus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cat_idestatus'], 'integer'],
            [['cat_nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CatEstatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cat_idestatus' => $this->cat_idestatus,
        ]);

        $query->andFilterWhere(['like', 'cat_nombre', $this->cat_nombre]);

        return $dataProvider;
    }
}
