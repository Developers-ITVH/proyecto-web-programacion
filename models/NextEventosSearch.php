<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextEventos;

/**
 * NextEventosSearch represents the model behind the search form of `app\models\NextEventos`.
 */
class NextEventosSearch extends NextEventos
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['eve_id'], 'integer'],
            [['eve_nombre', 'eve_ruta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextEventos::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'eve_id' => $this->eve_id,
        ]);

        $query->andFilterWhere(['like', 'eve_nombre', $this->eve_nombre])
            ->andFilterWhere(['like', 'eve_ruta', $this->eve_ruta]);

        return $dataProvider;
    }
}
