<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextEncargados;

/**
 * NextEncargadosSearch represents the model behind the search form of `app\models\NextEncargados`.
 */
class NextEncargadosSearch extends NextEncargados
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['enc_id', 'enc_fkgenero', 'enc_fkuserw'], 'integer'],
            [['enc_nombre', 'enc_paterno', 'enc_materno'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextEncargados::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'enc_id' => $this->enc_id,
            'enc_fkgenero' => $this->enc_fkgenero,
            'enc_fkuserw' => $this->enc_fkuserw,
        ]);

        $query->andFilterWhere(['like', 'enc_nombre', $this->enc_nombre])
            ->andFilterWhere(['like', 'enc_paterno', $this->enc_paterno])
            ->andFilterWhere(['like', 'enc_materno', $this->enc_materno]);

        return $dataProvider;
    }
}
