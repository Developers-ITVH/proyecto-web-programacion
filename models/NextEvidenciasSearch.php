<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextEvidencias;

/**
 * NextEvidenciasSearch represents the model behind the search form of `app\models\NextEvidencias`.
 */
class NextEvidenciasSearch extends NextEvidencias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evi_id', 'evi_fkcurso'], 'integer'],
            [['evi_nombre', 'evi_ruta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextEvidencias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'evi_id' => $this->evi_id,
            'evi_fkcurso' => $this->evi_fkcurso,
        ]);

        $query->andFilterWhere(['like', 'evi_nombre', $this->evi_nombre])
            ->andFilterWhere(['like', 'evi_ruta', $this->evi_ruta]);

        return $dataProvider;
    }
}
