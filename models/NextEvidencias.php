<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_evidencias".
 *
 * @property int $evi_id
 * @property string|null $evi_nombre
 * @property string|null $evi_ruta
 * @property int|null $evi_fkcurso
 *
 * @property NextCursos $eviFkcurso
 */
class NextEvidencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_evidencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['evi_id'], 'required'],
            [['evi_id', 'evi_fkcurso'], 'integer'],
            [['evi_nombre', 'evi_ruta'], 'string', 'max' => 255],
            [['evi_id'], 'unique'],
            [['evi_fkcurso'], 'exist', 'skipOnError' => true, 'targetClass' => NextCursos::className(), 'targetAttribute' => ['evi_fkcurso' => 'cur_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'evi_id' => 'Evi ID',
            'evi_nombre' => 'Evi Nombre',
            'evi_ruta' => 'Evi Ruta',
            'evi_fkcurso' => 'Evi Fkcurso',
        ];
    }

    /**
     * Gets query for [[EviFkcurso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEviFkcurso()
    {
        return $this->hasOne(NextCursos::className(), ['cur_id' => 'evi_fkcurso']);
    }
}
