<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\NextAsistencias;

/**
 * NextAsistenciasSearch represents the model behind the search form of `app\models\NextAsistencias`.
 */
class NextAsistenciasSearch extends NextAsistencias
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asi_id', 'asi_fkcurso', 'asi_fkestudiante'], 'integer'],
            [['asi_horaAsistencia', 'asi_fechaAsistencia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = NextAsistencias::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'asi_id' => $this->asi_id,
            'asi_horaAsistencia' => $this->asi_horaAsistencia,
            'asi_fechaAsistencia' => $this->asi_fechaAsistencia,
            'asi_fkcurso' => $this->asi_fkcurso,
            'asi_fkestudiante' => $this->asi_fkestudiante,
        ]);

        return $dataProvider;
    }
}
