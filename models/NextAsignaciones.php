<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "next_asignaciones".
 *
 * @property int $asg_id
 * @property string|null $asg_nombre
 * @property string|null $asg_indicaciones
 * @property string|null $asg_url
 * @property string|null $asg_fecha
 * @property int|null $asg_fkestatus
 * @property int|null $asg_fkcursos
 *
 * @property NextCursos $asgFkcursos
 * @property CatEstatus $asgFkestatus
 */
class NextAsignaciones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'next_asignaciones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asg_id'], 'required'],
            [['asg_id', 'asg_fkestatus', 'asg_fkcursos'], 'integer'],
            [['asg_indicaciones', 'asg_url'], 'string'],
            [['asg_fecha'], 'safe'],
            [['asg_nombre'], 'string', 'max' => 255],
            [['asg_id'], 'unique'],
            [['asg_fkcursos'], 'exist', 'skipOnError' => true, 'targetClass' => NextCursos::className(), 'targetAttribute' => ['asg_fkcursos' => 'cur_id']],
            [['asg_fkestatus'], 'exist', 'skipOnError' => true, 'targetClass' => CatEstatus::className(), 'targetAttribute' => ['asg_fkestatus' => 'cat_idestatus']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'asg_id' => 'Asg ID',
            'asg_nombre' => 'Asg Nombre',
            'asg_indicaciones' => 'Asg Indicaciones',
            'asg_url' => 'Asg Url',
            'asg_fecha' => 'Asg Fecha',
            'asg_fkestatus' => 'Asg Fkestatus',
            'asg_fkcursos' => 'Asg Fkcursos',
        ];
    }

    /**
     * Gets query for [[AsgFkcursos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsgFkcursos()
    {
        return $this->hasOne(NextCursos::className(), ['cur_id' => 'asg_fkcursos']);
    }

    /**
     * Gets query for [[AsgFkestatus]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsgFkestatus()
    {
        return $this->hasOne(CatEstatus::className(), ['cat_idestatus' => 'asg_fkestatus']);
    }
}
