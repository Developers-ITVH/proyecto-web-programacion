<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
  <div class="container">

    <a class="navbar-brand">
      <img src="images/inicio/nextLogo.png" class="d-inline-block align-top" alt="logo" width="35" height="30">
      NextCode
    </a>
    <button class="navbar-toggler btn-bar" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" [routerLinkActive]="['active']">Inicio</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" [routerLinkActive]="['active']">Cursos</a>
        </li>

        <li class="nav-item">
          <a class="nav-link"[routerLinkActive]="['active']">Ayuda</a>
        </li>

        <li class="nav-item">
          <a class="nav-link" [routerLinkActive]="['active']">Iniciar sesión</a>
        </li>

      </ul>
    </div>
  </div>
</nav>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="card footer">
  <div class="container text-white pt-5 pb-5">
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6">
        <p class="h4 card-title">NextCode</p>
        <p>Descubre tu potencial.</p>
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <p class="h4 card-title">Redes sociales</p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-facebook-square"></i> Facebook</a></p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-instagram"></i> Instagram</a></p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-youtube"></i> Youtube</a></p>
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <p class="h4 card-title">Ayuda</p>
        <p class="text-white"><i class="far fa-envelope"></i> nextcode@email.com </p>
        <p class="text-white"><i class="fas fa-phone"></i> 993-403-5824</p>
      </div>
    </div>
  </div>
  <div class="card-footer text-muted">
    <div class="container text-center">
      © 2020 Copyright: nextcode.tech
    </div>
  </div>
</footer>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
