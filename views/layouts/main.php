<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\FormAsset;
use webvimark\modules\UserManagement\UserManagementModule;
use webvimark\modules\UserManagement\models\User;

app\assets\FormAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
           
<?php $this->head() ?>
<!-- ======= Header ======= -->
  <header id="header" class="fixed-top header">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light"><a href="/"><p>NextCode</p></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!--<a href="index.html"><img src="images/inicio/nextLogo-BN.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="/">Inicio</a></li>
          <li><a href="/site/cursos">Cursos</a></li>
          <li><a href="/site/eventos">Eventos</a></li>
          <li><a href="services.html">Ayuda</a></li>
          <li><a href="team.html">Acerca de</a></li>
          <?php if(User::hasRole([''],$superAdminAllowed=true)){ ?>
          <li class="drop-down"><a href="">Administrar</a>
            <ul>
              <li><a href="/next-cursos">Administrar Cursos</a></li>
              <li><a href="/next-estudiantes">Administrar Estudiantes</a></li>
              <li><a href="/next-encargados">Administrar Asesores</a></li>
              <li><a href="/next-eventos">Administrar Eventos</a></li>
              <li><a href="/next-evidencias">Administrar Evidencias</a></li>
            </ul>
          </li>
          <li class="drop-down"><a href="#">Rutas</a>
                <ul>
                  <li><a href="/user-management/user/index">Usuarios</a></li>
                  <li><a href="/user-management/role/index">Roles</a></li>
                  <li><a href="/user-management/permission/index">Permisos</a></li>
                  <li><a href="/user-management/auth-item-group/index">Grupos de permisos</a></li>
                </ul>
          </li>
         <?php } if(Yii::$app->user->isGuest == true) { ?>
          <li class="drop-down"><a href="#"><i class="fas fa-user-circle"></i></a>
                <ul>
                  <li><a class="nav-link" href="/user-management/auth/login">Iniciar sesión</a></li>
                  <li><a class="nav-link" href="/next-estudiantes/registro">Registrarse</a></li>
                </ul>
          </li>
          <?php }else { ?>
          <li class="drop-down"><a href="#"><i class="fas fa-user-circle"></i></a>
                <ul>
                 <li><a class="nav-link" href="/user-management/auth/logout">Cerrar sesión</a></li>
                </ul>
          </li>
          <?php } ?>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

</head>  
<?php $this->beginBody() ?>

<div class="container" style="margin-top: 10%;">

<?= $content ?>

</div>

</div>



<?php $this->endBody() ?>
<!-- ======= Footer ======= -->
<footer class="card footer">
  <div class="container text-white pt-5 pb-5">
    <div class="row">
      <div class="col-sm-12 col-md-6 col-lg-6">
        <p class="h4 card-title">NextCode</p>
        <p>Descubre tu potencial.</p>
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <p class="h4 card-title">Redes sociales</p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-facebook-square"></i> Facebook</a></p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-instagram"></i> Instagram</a></p>
        <p><a href="#" class="list-redes text-white"><i class="fab fa-youtube"></i> Youtube</a></p>
      </div>
      <div class="col-sm-12 col-md-3 col-lg-3 col-xl-3">
        <p class="h4 card-title">Ayuda</p>
        <p class="text-white"><i class="far fa-envelope"></i> nextcode@email.com </p>
        <p class="text-white"><i class="fas fa-phone"></i> 993-403-5824</p>
      </div>
    </div>
  </div>
  <div class="card-footer text-muted">
    <div class="container text-center">
      © 2020 Copyright: nextcode.tech
    </div>
  </div>
</footer><!-- End Footer -->
</html>
<?php $this->endPage() ?>


<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>