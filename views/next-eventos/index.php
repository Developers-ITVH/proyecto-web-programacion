<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextEventosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Next Eventos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-eventos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Next Eventos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'eve_id',
            'eve_nombre',
            'eve_ruta',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
