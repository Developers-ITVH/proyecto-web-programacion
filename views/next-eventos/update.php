<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEventos */

$this->title = 'Update Next Eventos: ' . $model->eve_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->eve_id, 'url' => ['view', 'id' => $model->eve_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-eventos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
