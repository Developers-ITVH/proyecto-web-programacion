<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEventos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-eventos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'eve_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'eve_ruta')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
