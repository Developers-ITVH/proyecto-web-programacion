<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEventos */

$this->title = 'Create Next Eventos';
$this->params['breadcrumbs'][] = ['label' => 'Next Eventos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-eventos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
