<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextInscripcion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-inscripcion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'inc_fkcurso')->textInput() ?>

    <?= $form->field($model, 'inc_fkestudiante')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
