<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextInscripcion */

$this->title = 'Create Next Inscripcion';
$this->params['breadcrumbs'][] = ['label' => 'Next Inscripcions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-inscripcion-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
