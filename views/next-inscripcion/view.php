<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\NextInscripcion */

$this->title = $model->Curso;

app\assets\HomeAsset::register($this);
?>
<div class="wer" style="background: #393d40">
  <div class="anim-text-banner">
    <span class="anim-text">public class NextCode { </span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("Bienvenid@s");</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
    <br>
    <div class="anim-text-parent">
      <span class="anim-text">}</span>
  <!--    <span class="anim-text">add new line here</span> -->
      <span class="v-bar"></span>
    </div>
    <br>
    <span class="anim-text">public class Curso { </span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("<?=$model->Curso?>");</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
    <br>
    <div class="anim-text-parent">
      <span class="anim-text">}</span>
  <!--    <span class="anim-text">add new line here</span> -->
      <span class="v-bar"></span>
    </div>
    <br>
    <span class="anim-text">public class Estudiante inscrito { </span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("<?=$model->Nombrecom?>");</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
    <br>
    <div class="anim-text-parent">
      <span class="anim-text">}</span>
  <!--    <span class="anim-text">add new line here</span> -->
      <span class="v-bar"></span>
    </div>
    <br>
    <span class="anim-text">public class Botón { </span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?= Html::a('Ver mis cursos', ['create'], ['class' => 'btn btn-danger', 'style' => 'text-align: text-left;']) ?></span>
    <br>
    <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
    <br>
    <div class="anim-text-parent">
      <span class="anim-text">}</span>
      <span class="anim-text">}</span>
      <span class="anim-text">}</span>
  <!--    <span class="anim-text">add new line here</span> -->
      <span class="v-bar"></span>
    </div>
    <br>
  </div>

</div>



<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery/jquery.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.easing.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/counterup.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/venobox.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.waypoints.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/aos.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>


