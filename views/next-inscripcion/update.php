<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextInscripcion */

$this->title = 'Update Next Inscripcion: ' . $model->inc_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Inscripcions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->inc_id, 'url' => ['view', 'id' => $model->inc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-inscripcion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
