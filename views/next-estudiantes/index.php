<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextEstudiantesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Next Estudiantes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-estudiantes-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear nuevo estudiante', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'est_id',
            'est_nombre',
            'est_paterno',
            'est_materno',
            'est_matricula',
            'Carrera',
            'Genero',
            //'est_correo',
            //'est_fkcarrera',
            //'est_fkgenero',
            //'est_fkuserw',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
