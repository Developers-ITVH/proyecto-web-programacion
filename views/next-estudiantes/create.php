<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEstudiantes */

$this->title = 'Create Next Estudiantes';
$this->params['breadcrumbs'][] = ['label' => 'Next Estudiantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-estudiantes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
