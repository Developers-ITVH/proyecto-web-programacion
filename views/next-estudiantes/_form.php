<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEstudiantes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-estudiantes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'est_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'est_paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'est_materno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'est_matricula')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'est_correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'est_fkcarrera')->textInput() ?>

    <?= $form->field($model, 'est_fkgenero')->textInput() ?>

    <?= $form->field($model, 'est_fkuserw')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
