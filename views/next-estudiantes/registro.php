<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\NextEstudiantes */
/* @var $form ActiveForm */
?>
<div class="registro">
<h2>Registrarse</h2>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'est_nombre') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'est_paterno') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'est_materno') ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'est_matricula') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'est_fkcarrera')->widget(Select2::classname(), 
                  ['data'    => $carrera,
                  'options' => ['placeholder' => 'Selecciona una carrera...'],
                  'pluginOptions' => ['allowClear' => true],
               ]);
            ?>
        </div>
        <div class="col-lg-6">
             <?= $form->field($model, 'est_fkgenero')->widget(Select2::classname(), 
                  ['data'    => $genero,
                  'options' => ['placeholder' => 'Selecciona un genero...'],
                  'pluginOptions' => ['allowClear' => true],
               ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($user, 'password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?> 
        </div>
        <div class="col-lg-6">
            <?= $form->field($user, 'repeat_password')->passwordInput(['maxlength' => 255, 'autocomplete'=>'off']) ?>
        </div>       
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($user, 'email')->textInput(['placeholder' => 'Ingrese su correo electrónico', 'autocomplete'=>'off', 'autofocus'=>true]) ?>
        </div> 
    </div>
    
        <div class="form-group">
            <?= Html::submitButton('Registrarse', ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- registro -->
