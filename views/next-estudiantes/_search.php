<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEstudiantesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-estudiantes-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'est_id') ?>

    <?= $form->field($model, 'est_nombre') ?>

    <?= $form->field($model, 'est_paterno') ?>

    <?= $form->field($model, 'est_materno') ?>

    <?= $form->field($model, 'est_matricula') ?>

    <?php // echo $form->field($model, 'est_correo') ?>

    <?php // echo $form->field($model, 'est_fkcarrera') ?>

    <?php // echo $form->field($model, 'est_fkgenero') ?>

    <?php // echo $form->field($model, 'est_fkuserw') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
