<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NextEstudiantes */

$this->title = $model->est_nombre;
\yii\web\YiiAsset::register($this);
?>
<div class="next-estudiantes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Editar', ['update', 'id' => $model->est_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->est_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'est_id',
            'est_nombre',
            'est_paterno',
            'est_materno',
            'est_matricula',
            'est_correo',
            'Carrera',
            'Genero',
            //'est_fkuserw',
        ],
    ]) ?>

</div>
