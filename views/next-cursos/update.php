<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextCursos */

$this->title = 'Update Next Cursos: ' . $model->cur_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Cursos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cur_id, 'url' => ['view', 'id' => $model->cur_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-cursos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'encargado' => $encargado,
        'estatus' => $estatus
    ]) ?>

</div>
