<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextCursos */

$this->title = 'Crear nuevo curso';

?>
<div class="next-cursos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'encargado' => $encargado,
        'estatus' => $estatus,
    ]) ?>

</div>
