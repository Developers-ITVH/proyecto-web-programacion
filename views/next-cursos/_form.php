<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker ;
use kartik\file\FileInput;
use kartik\time\TimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\NextCursos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-cursos-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-4">
            <?= $form->field($model, 'cur_nombre')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'cur_fkencargados')->widget(Select2::classname(), 
                  ['data'    => $encargado,
                  'options' => ['placeholder' => 'Selecciona un encargado...'],
                  'pluginOptions' => ['allowClear' => true],
               ]);
            ?>
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'cur_fkestatus')->widget(Select2::classname(), 
                  ['data'    => $estatus,
                  'options' => ['placeholder' => 'Selecciona un estatus...'],
                  'pluginOptions' => ['allowClear' => true],
               ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'cur_descripcion')->textarea(['rows' => 6]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <?= $form->field($model, 'cur_foto') ->widget(FileInput::classname(), [
                        'pluginOptions' => [
                            'showCaption' => false,
                            'showRemove' => true,
                            'showUpload' => false,
                            'removeClass' => 'btn btn-danger',
                            'browseClass' => 'btn btn-info btn-block',
                            'browseIcon' => '<i class="fas fa-camera"></i> ',
                            'browseLabel' =>  'Selecciona foto',
                        ],
                    'options' => ['accept' => 'image/*']
                ]);   
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'cur_horaInicial')->widget(TimePicker::classname(),[
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => true,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); 
        ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'cur_horaFinal')->widget(TimePicker::classname(),[
                'pluginOptions' => [
                    'showSeconds' => true,
                    'showMeridian' => true,
                    'minuteStep' => 1,
                    'secondStep' => 5,
                ]
            ]); 
        ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?= $form->field($model, 'cur_fechaInicial')->widget(DatePicker::classname (), [  
                    'options' => [ 'placeholder' => 'Ingrese la fecha inicial del curso ...','class'=>'contact-input', ],    
                    'pluginOptions' => [                   
                    'autoclose' =>true                   
                            
                    ]
                ]); 
            ?>
        </div>
        <div class="col-lg-6">
            <?= $form->field($model, 'cur_fechaFinal')->widget(DatePicker::classname (), [  
                    'options' => [ 'placeholder' => 'Ingrese la fecha final del curso ...','class'=>'contact-input', ],    
                    'pluginOptions' => [                   
                    'autoclose' =>true                   
                            
                    ]
                ]); 
            ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Crear curso', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
