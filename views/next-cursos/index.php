<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextCursosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cursos';
?>
<div class="next-cursos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Cursos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="cursos" style="width: 100%; overflow: scroll;">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'cur_id',
            'cur_nombre',
            'cur_descripcion:ntext',
            'cur_foto',
            'cur_horaInicial',
            'cur_horaFinal',
            'cur_fechaInicial',
            'cur_fechaFinal',
            'cur_fechaCreacion',
            'cur_fkestatus',
            'cur_fkencargados',
            //'cur_asistenciasestatus',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>

</div>
