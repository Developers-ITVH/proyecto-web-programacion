<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextCursosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-cursos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cur_id') ?>

    <?= $form->field($model, 'cur_nombre') ?>

    <?= $form->field($model, 'cur_descripcion') ?>

    <?= $form->field($model, 'cur_foto') ?>

    <?= $form->field($model, 'cur_horario') ?>

    <?php // echo $form->field($model, 'cur_fechaInicial') ?>

    <?php // echo $form->field($model, 'cur_fechaFinal') ?>

    <?php // echo $form->field($model, 'cur_fechaCreacion') ?>

    <?php // echo $form->field($model, 'cur_fkestatus') ?>

    <?php // echo $form->field($model, 'cur_fkencargados') ?>

    <?php // echo $form->field($model, 'cur_asistenciasestatus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
