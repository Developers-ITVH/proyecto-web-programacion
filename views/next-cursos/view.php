<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NextCursos */

$this->title = $model->cur_nombre;

\yii\web\YiiAsset::register($this);
?>
<div class="next-cursos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->cur_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->cur_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'cur_id',
            [
                'attribute' => 'cur_foto',
                'format' => 'raw',
                'value' => function ($model) {
                if ($model->cur_foto!='')
                return '<img src="'.Yii::$app->homeUrl. $model->cur_foto.'" width="40%" height="auto">'; else return 'no image';
                },
            ],
            'cur_nombre',
            'cur_descripcion:ntext',
            'Encargado',
            'cur_horaInicial',
            'cur_horaFinal',
            'cur_fechaInicial',
            'cur_fechaFinal',
            'cur_fechaCreacion',
            'Estatus',
            //'cur_asistenciasestatus',
        ],
    ]) ?>

</div>
