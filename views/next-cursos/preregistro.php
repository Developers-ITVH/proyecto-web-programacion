<?php
use app\assets\HomeAsset;
use yii\helpers\Html;
/* @var $this yii\web\View */


app\assets\HomeAsset::register($this);
?>

  <main id="main">

    <!-- ======= Blog Section ======= -->
    <section class="blog" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 entries">

            <article class="entry">

              <div class="entry-img">
                <img src="<?='/'.$curso->cur_foto?>" alt="" class="img-fluid">
              </div>

              <h2 class="entry-title">
                <a href="blog-single.html"><?=$curso->cur_nombre?></a>
              </h2>

              <div class="entry-meta">
                <ul>
                  <li class="d-flex align-items-center"><i class="icofont-user"></i> <a href="blog-single.html">Asesor: <?= $curso->Encargado?></a></li>
                  <li class="d-flex align-items-center"><i class="icofont-wall-clock"></i> <a href="blog-single.html"><time datetime="2020-01-01">Fecha de inicio: <?= $curso->cur_fechaInicial.'  -  '.'Fecha final:'.$curso->cur_fechaFinal?></time></a></li>
                </ul>
              </div>

              <div class="entry-content">
                <p>
                  <?=$curso->cur_descripcion?>
                </p>
                <div class="read-more">
                  <?= Html::a('<i class="icofont-arrow-right"></i> Inscribirse', ['/next-estudiantes/inscripcion','id'=>$curso->cur_id]); ?>
                </div>
              </div>

            </article><!-- End blog entry -->

          </div><!-- End blog entries list -->

          <div class="col-lg-4">
            <div class="sidebar">
              <h3 class="sidebar-title">Cursos sugeridos</h3>
              <div class="sidebar-item recent-posts">
                <?php foreach ($todos as $tod => $todo) { ?>
                <div class="post-item clearfix">
                  <img src="<?= '/'.$todo->cur_foto?>" alt="">
                  <h4><a href="blog-single.html"><?=$todo->cur_nombre?></a></h4>
                  <time datetime="2020-01-01"><?=$todo->cur_fechaInicial;?></time>
                </div>
              <?php } ?>
              </div><!-- End sidebar recent posts-->

            </div><!-- End sidebar -->

          </div><!-- End blog sidebar -->

        </div><!-- End .row -->

      </div><!-- End .container -->

    </section><!-- End Blog Section -->

  </main> <!-- End #main -->


<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery/jquery.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.easing.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/counterup.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/venobox.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.waypoints.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/aos.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>


