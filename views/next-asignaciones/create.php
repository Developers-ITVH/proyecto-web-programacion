<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsignaciones */

$this->title = 'Create Next Asignaciones';
$this->params['breadcrumbs'][] = ['label' => 'Next Asignaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-asignaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
