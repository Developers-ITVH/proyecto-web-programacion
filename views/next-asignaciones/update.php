<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsignaciones */

$this->title = 'Update Next Asignaciones: ' . $model->asg_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Asignaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asg_id, 'url' => ['view', 'id' => $model->asg_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-asignaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
