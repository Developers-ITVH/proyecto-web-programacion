<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsignaciones */

$this->title = $model->asg_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Asignaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="next-asignaciones-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->asg_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->asg_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'asg_id',
            'asg_nombre',
            'asg_indicaciones:ntext',
            'asg_url:ntext',
            'asg_fecha',
            'asg_fkestatus',
            'asg_fkcursos',
        ],
    ]) ?>

</div>
