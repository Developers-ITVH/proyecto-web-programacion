<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsignacionesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-asignaciones-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'asg_id') ?>

    <?= $form->field($model, 'asg_nombre') ?>

    <?= $form->field($model, 'asg_indicaciones') ?>

    <?= $form->field($model, 'asg_url') ?>

    <?= $form->field($model, 'asg_fecha') ?>

    <?php // echo $form->field($model, 'asg_fkestatus') ?>

    <?php // echo $form->field($model, 'asg_fkcursos') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
