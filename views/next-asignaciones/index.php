<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextAsignacionesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Next Asignaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-asignaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Next Asignaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'asg_id',
            'asg_nombre',
            'asg_indicaciones:ntext',
            'asg_url:ntext',
            'asg_fecha',
            //'asg_fkestatus',
            //'asg_fkcursos',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
