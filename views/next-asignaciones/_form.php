<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsignaciones */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-asignaciones-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'asg_id')->textInput() ?>

    <?= $form->field($model, 'asg_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'asg_indicaciones')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'asg_url')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'asg_fecha')->textInput() ?>

    <?= $form->field($model, 'asg_fkestatus')->textInput() ?>

    <?= $form->field($model, 'asg_fkcursos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
