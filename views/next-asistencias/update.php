<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsistencias */

$this->title = 'Update Next Asistencias: ' . $model->asi_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Asistencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->asi_id, 'url' => ['view', 'id' => $model->asi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-asistencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
