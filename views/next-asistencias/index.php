<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextAsistenciasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Next Asistencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-asistencias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Next Asistencias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'asi_id',
            'asi_horaAsistencia',
            'asi_fechaAsistencia',
            'asi_fkcurso',
            'asi_fkestudiante',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
