<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsistencias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-asistencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'asi_id')->textInput() ?>

    <?= $form->field($model, 'asi_horaAsistencia')->textInput() ?>

    <?= $form->field($model, 'asi_fechaAsistencia')->textInput() ?>

    <?= $form->field($model, 'asi_fkcurso')->textInput() ?>

    <?= $form->field($model, 'asi_fkestudiante')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
