<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsistencias */

$this->title = 'Create Next Asistencias';
$this->params['breadcrumbs'][] = ['label' => 'Next Asistencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-asistencias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
