<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextAsistenciasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-asistencias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'asi_id') ?>

    <?= $form->field($model, 'asi_horaAsistencia') ?>

    <?= $form->field($model, 'asi_fechaAsistencia') ?>

    <?= $form->field($model, 'asi_fkcurso') ?>

    <?= $form->field($model, 'asi_fkestudiante') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
