<?php
use app\assets\HomeAsset;
/* @var $this yii\web\View */


app\assets\HomeAsset::register($this);
?>
<div class="site-index">

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Bienvenidos a NextCode</h2>
          <p class="animate__animated animate__fadeInUp"><div class="anim-text-banner">
                  <span class="anim-text">public class NextCode { </span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("NextCode");</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
                  <br>
                  <div class="anim-text-parent">
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                <!--    <span class="anim-text">add new line here</span> -->
                    <span class="v-bar"></span>
                  </div>
                  <br>
                </div></p>
          <a href="/" class="btn-get-started animate__animated animate__fadeInUp">Regresar</a>
        </div>
      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Our Services Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Participaciones en eventos</h2>
        </div>

      </div>
  <!-- ======= Portfolio Section ======= -->
    <section class="portfolio">
      <div class="container">

        <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">
          <?php
                foreach (glob("images/participaciones/*.jpg") as $filename) { 
                ?>
          <div class="col-lg-4 col-md-6 filter-web">
            <div class="portfolio-item">
              <img src="<?='../'.$filename?>" class="img-fluid" alt="" style="width: 100%; height: 201px;">
              <div class="portfolio-info">
                <h3><a href="<?='../'.$filename?>" data-gall="portfolioGallery" class="venobox" title="App 1">App 1</a></h3>
                <div>
                  <a href="<?='../'.$filename?>" data-gall="portfolioGallery" class="venobox" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" title="Portfolio Details"><i class="bx bx-link"></i></a>
                </div>
              </div>
            </div>
          </div>
    <?php } ?>

        </div>

      </div>
    </section><!-- End Portfolio Section -->
  </main><!-- End #main -->


  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>


<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery/jquery.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.easing.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/counterup.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/venobox.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.waypoints.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/aos.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>

