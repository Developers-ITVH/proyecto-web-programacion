<?php
use app\assets\HomeAsset;
/* @var $this yii\web\View */


app\assets\HomeAsset::register($this);
?>
<div class="site-index">

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Bienvenidos a NextCode</h2>
          <p class="animate__animated animate__fadeInUp"><div class="anim-text-banner">
                  <span class="anim-text">public class NextCode { </span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("NextCode");</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
                  <br>
                  <div class="anim-text-parent">
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                <!--    <span class="anim-text">add new line here</span> -->
                    <span class="v-bar"></span>
                  </div>
                  <br>
                </div></p>
          <a href="site/cursos" class="btn-get-started animate__animated animate__fadeInUp">Ver cursos</a>
        </div>
      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">

    <!-- ======= Services Section ======= -->
    <section class="services">
      <div class="container">

        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
               <img style="width: 50%" src="images/inicio/java.png">
              <h4 class="title"><a href="">JAVA</a></h4>
              <p class="description">Es un lenguaje de programación de propósito general, uno de los más populares y con mayores aplicaciones del panorama actual.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
               <img style="width: 50%" src="images/inicio/python.png">
              <h4 class="title"><a href="">PYTHON</a></h4>
              <p class="description">Es un lenguaje de programación interpretado cuya filosofía hace hincapié en la legibilidad de su código.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <img style="width: 50%" src="images/inicio/php.png">
              <h4 class="title"><a href="">PHP</a></h4>
              <p class="description">Es un lenguaje de programación de uso general que se adapta especialmente al desarrollo web.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
             <img style="width: 50%" src="images/inicio/mysql.png">
              <h4 class="title"><a href="">MYSQL</a></h4>
              <p class="description">Es un sistema de gestión de bases de datos relacional desarrollado bajo licencia dual.</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->
    <!-- ======= Features Section ======= -->
    <section class="features">
      <div class="container">
        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="images/inicio/mision.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-4">
            <h3>Misión</h3>
            <p class="font-italic">
              Ser una comunidad de desarrolladores de software, en la que se promueva el uso y aplicación de las nuevas tecnologías para dar solución a diversos problemas en nuestro entorno.
            </p>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5 order-1 order-md-2">
            <img src="images/inicio/vision.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5 order-2 order-md-1">
            <h3>Visión</h3>
            <p class="font-italic">
              Formar y/o capacitar a futuros desarrolladores de software con los conocimientos, habilidades y capacidades necesarias para la solución de diversos problemas implementando el uso de los diferentes lenguajes y/o herramientas de programación.
            </p>
          </div>
        </div>

        <div class="row" data-aos="fade-up">
          <div class="col-md-5">
            <img src="images/inicio/objetivos.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-7 pt-5">
            <h3>Objetivos.</h3>
            <ul>
              <li><i class="icofont-check"></i>Brindar apoyo a aquellos alumnos con dificultades en las diversas áreas o campos del desarrollo de software.</li>
              <li><i class="icofont-check"></i>Formar una comunidad enfocada al desarrollo de software, en la que se fomente el trabajo en equipo.</li>
              <li><i class="icofont-check"></i>Ser una comunidad de innovación, creando proyectos que resuelvan problemáticas que puedan ser aplicadas a situaciones actuales.</li>
              <li><i class="icofont-check"></i>Hacer de NextCode un ambiente para que los desarrolladores convivan y compartan sus conocimientos.</li>
            </ul>
          </div>
        </div>
    </section><!-- End Features Section -->

  </main><!-- End #main -->


  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>


<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery/jquery.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.easing.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/counterup.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/venobox.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.waypoints.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/aos.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>

