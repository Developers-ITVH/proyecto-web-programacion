<?php
use app\assets\HomeAsset;
use yii\helpers\Html;
/* @var $this yii\web\View */


app\assets\HomeAsset::register($this);
?>
<div class="site-index">

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex justify-cntent-center align-items-center">
    <div id="heroCarousel" class="container carousel carousel-fade" data-ride="carousel">

      <!-- Slide 1 -->
      <div class="carousel-item active">
        <div class="carousel-container">
          <h2 class="animate__animated animate__fadeInDown">Bienvenidos a NextCode</h2>
          <p class="animate__animated animate__fadeInUp"><div class="anim-text-banner">
                  <span class="anim-text">public class NextCode { </span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;public static void main(String args[]) {</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;System.out.println("NextCode");</span>
                  <br>
                  <span class="anim-text">&nbsp;&nbsp;&nbsp;&nbsp;} </span>
                  <br>
                  <div class="anim-text-parent">
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                    <span class="anim-text">}</span>
                <!--    <span class="anim-text">add new line here</span> -->
                    <span class="v-bar"></span>
                  </div>
                  <br>
                </div></p>
          <a href="/" class="btn-get-started animate__animated animate__fadeInUp">Regresar</a>
        </div>
      </div>

      <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon bx bx-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>

      <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon bx bx-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>

    </div>
  </section><!-- End Hero -->

  <main id="main">
    <!-- ======= Our Services Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Nuestros cursos</h2>
        </div>

      </div>
    </section><!-- End Our Services Section -->
      <!-- ======= Service Details Section ======= -->
    <section class="service-details">
      <div class="container">

        <div class="row">
          <?php foreach ($curso as $cur => $cursos) { ?>
          <div class="col-md-6 d-flex align-items-stretch" data-aos="fade-up">
            <div class="card">
              <div class="card-img">
                <img src="<?= '/'.$cursos->cur_foto?>" alt="...">
              </div>
              <div class="card-body">
                <h5 class="card-title"><a href="#"><?= $cursos->cur_nombre?></a></h5>
                <p class="card-text"><?= $cursos->cur_descripcion?></p>
                <p class="card-text">Asesor: <?= $cursos->Encargado?></p>
                <p class="card-text">Fecha de inicio: <?= $cursos->cur_fechaInicial.'  -  '.'Fecha final:'.$cursos->cur_fechaFinal?> <p class="card-text"></p></p>
                <p class="card-text">Hora de inicio: <?= $cursos->cur_horaInicial.'  -  '.'Hora final:'.$cursos->cur_horaFinal?> <p class="card-text"></p></p>
                <div class="read-more"><?= Html::a('<i class="icofont-arrow-right"></i> Ver curso', ['/next-cursos/preregistro','id'=>$cursos->cur_id]); ?></div>
              </div>
            </div>
          </div>
        <?php } ?>
        </div>

      </div>
    </section><!-- End Service Details Section -->


  </main><!-- End #main -->


  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>


<script src="<?= \Yii::getAlias('@web/js/bootstrap/bootstrap.bundle.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery/jquery.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.easing.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/counterup.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/owl.carousel.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/isotope.pkgd.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/venobox.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/jquery.waypoints.min.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/aos.js');?>"></script>
<script src="<?= \Yii::getAlias('@web/js/main_home.js');?>"></script>

