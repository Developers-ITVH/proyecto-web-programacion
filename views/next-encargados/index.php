<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextEncargadosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lista de Encargados';

?>
<div class="next-encargados-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Crear Encargados', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'enc_id',
            'enc_nombre',
            'enc_paterno',
            'enc_materno',
            'Genero',
            //'enc_fkuserw',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
