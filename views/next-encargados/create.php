<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEncargados */

$this->title = 'Crear nuevo encargado';
?>
<div class="next-encargados-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'genero' => $genero
    ]) ?>

</div>
