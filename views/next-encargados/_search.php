<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEncargadosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-encargados-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'enc_id') ?>

    <?= $form->field($model, 'enc_nombre') ?>

    <?= $form->field($model, 'enc_paterno') ?>

    <?= $form->field($model, 'enc_materno') ?>

    <?= $form->field($model, 'enc_fkgenero') ?>

    <?php // echo $form->field($model, 'enc_fkuserw') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
