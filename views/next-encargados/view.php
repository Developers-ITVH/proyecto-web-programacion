<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\NextEncargados */

$this->title = $model->enc_nombre;

\yii\web\YiiAsset::register($this);
?>
<div class="next-encargados-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->enc_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->enc_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'enc_id',
            'enc_nombre',
            'enc_paterno',
            'enc_materno',
            'Genero',
            //'enc_fkuserw',
        ],
    ]) ?>

</div>
