<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEncargados */

$this->title = 'Update Next Encargados: ' . $model->enc_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Encargados', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->enc_id, 'url' => ['view', 'id' => $model->enc_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-encargados-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'user' => $user,
        'genero' => $genero
    ]) ?>

</div>
