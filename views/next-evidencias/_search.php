<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEvidenciasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-evidencias-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'evi_id') ?>

    <?= $form->field($model, 'evi_nombre') ?>

    <?= $form->field($model, 'evi_ruta') ?>

    <?= $form->field($model, 'evi_fkcurso') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
