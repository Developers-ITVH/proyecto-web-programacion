<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEvidencias */

$this->title = 'Update Next Evidencias: ' . $model->evi_id;
$this->params['breadcrumbs'][] = ['label' => 'Next Evidencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->evi_id, 'url' => ['view', 'id' => $model->evi_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="next-evidencias-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
