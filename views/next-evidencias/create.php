<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\NextEvidencias */

$this->title = 'Create Next Evidencias';
$this->params['breadcrumbs'][] = ['label' => 'Next Evidencias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-evidencias-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
