<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\NextEvidenciasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Next Evidencias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="next-evidencias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Next Evidencias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'evi_id',
            'evi_nombre',
            'evi_ruta',
            'evi_fkcurso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
