<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NextEvidencias */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="next-evidencias-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'evi_id')->textInput() ?>

    <?= $form->field($model, 'evi_nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evi_ruta')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'evi_fkcurso')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
