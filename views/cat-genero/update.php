<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CatGenero */

$this->title = 'Update Cat Genero: ' . $model->cat_idgenero;
$this->params['breadcrumbs'][] = ['label' => 'Cat Generos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cat_idgenero, 'url' => ['view', 'id' => $model->cat_idgenero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cat-genero-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
