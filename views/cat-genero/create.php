<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CatGenero */

$this->title = 'Create Cat Genero';
$this->params['breadcrumbs'][] = ['label' => 'Cat Generos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cat-genero-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
