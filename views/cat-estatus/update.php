<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CatEstatus */

$this->title = 'Update Cat Estatus: ' . $model->cat_idestatus;
$this->params['breadcrumbs'][] = ['label' => 'Cat Estatuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cat_idestatus, 'url' => ['view', 'id' => $model->cat_idestatus]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cat-estatus-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
